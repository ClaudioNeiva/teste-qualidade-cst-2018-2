package br.ucsal.cst20182.testequalidade.aula10.exemplo2;

import java.io.PrintStream;
import java.util.Scanner;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

public class TuiUtilUnitarioTest {

	@Test
	public void testarObterNomeCompleto() {

		// Setup
		// Instanciar um scannerMock para ensinar os retornos que desejo.
		TuiUtil tuiUtil = new TuiUtil();
		Scanner scannerMock = Mockito.mock(Scanner.class);
		tuiUtil.scanner = scannerMock;

		// Definir dados de entrada
		String nome = "Claudio";
		String sobrenome = "Neiva";
		Mockito.doReturn(nome).doReturn(sobrenome).when(scannerMock).nextLine();

		// Definir sa�da esperada
		String nomeCompletoEsperado = "Claudio Neiva";

		// Executar o m�todo a ser testado e obter o resultado atual
		String nomeCompletoAtual = tuiUtil.obterNomeCompleto();

		// Comparar o resultado esperado com o resultado atual
		Assert.assertEquals(nomeCompletoEsperado, nomeCompletoAtual);
	}

	@Test
	public void testarExibirMensagem() {
		// Setup
		TuiUtil tuiUtil = new TuiUtil();
		
		// Construir um mock para substituir o console.
		PrintStream printStreamMock = Mockito.mock(PrintStream.class);
		System.setOut(printStreamMock);
		
		// Definir os dados de entrada
		String mensagem = "Tem que estudar para poder aprender!";

		// Chamar o m�todo que est� sendo testado (mas ele � "command" e,
		// portanto, n�o retorna nada).
		tuiUtil.exibirMensagem(mensagem);

		// Verificar se foi chamado o m�todo println passando tanto "Bom dia!"
		// quanto a mensagem atual.
		Mockito.verify(printStreamMock).println("Bom dia!");
		Mockito.verify(printStreamMock).println(mensagem);
	}
}
