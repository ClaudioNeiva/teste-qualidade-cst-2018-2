package br.ucsal.cst20182.testequalidade.aula09;

// Executar o maven no projeto (Run as -> Maven Build...)
// Utilizar os objetivos (goals): clean verify sonar:sonar
// Se tiver problemas com testes unit�rios, pode utilizar a op��o Skip Tests.

public class AlunoTui {

	private static AlunoDAO alunoDAO = new AlunoDAO();

	private static DateUtil dateUtil = new DateUtil();

	private static AlunoBO alunoBO = new AlunoBO(alunoDAO, dateUtil);

	public static void main(String[] args) {
		cadastrarAluno();
		consultarAluno();
	}

	private static void cadastrarAluno() {
		Aluno aluno1 = new Aluno();
		aluno1.setMatricula(876);
		aluno1.setNome("Claudio Neiva");
		aluno1.setSituacao(SituacaoAluno.ATIVO);
		aluno1.setAnoNascimento(2001);

		alunoBO.atualizar(aluno1);
	}

	private static void consultarAluno() {
		System.out.println("idade do aluno de matr�cula 876=" + alunoBO.calcularIdade(876));
	}

}
