package br.ucsal.cst20182.testequalidade.aula09;

public class AlunoDAOStub extends AlunoDAO {

	@Override
	public void salvar(Aluno aluno) {
	}

	@Override
	public Aluno encontrarPorMatricula(Integer matricula) {
		if (matricula.equals(5)) {
			return AlunoBuilder.umAlunoAtivo().comMatricula(matricula).nascidoEm(1998).build();
		}
		return null;
	}

}
