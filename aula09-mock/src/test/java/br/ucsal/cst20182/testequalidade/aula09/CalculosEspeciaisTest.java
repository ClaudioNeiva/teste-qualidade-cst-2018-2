package br.ucsal.cst20182.testequalidade.aula09;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CalculosEspeciaisTest {

	public CalculosEspeciais calculosEspeciais;

	@Before
	public void setup() {
		// CalculoUtil calculoUtil = new CalculoUtil();
		CalculoUtil calculoUtil = new CalculoUtilDuble();
		calculosEspeciais = new CalculosEspeciais(calculoUtil);
	}

	@Test
	public void testarE3() {
		Integer n = 3;

		Double eEsperado = 2.66;

		Double eAtual = calculosEspeciais.calcularE(n);

		Assert.assertEquals(eEsperado, eAtual, 0.01);
	}

}
