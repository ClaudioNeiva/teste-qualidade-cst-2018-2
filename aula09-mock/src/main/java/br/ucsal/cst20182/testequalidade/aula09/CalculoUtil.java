package br.ucsal.cst20182.testequalidade.aula09;

public class CalculoUtil {

	public Long calcularFatorial(int n) {
		Long fat = 1L;
		for (int i = 1; i <= n; i++) {
			fat *= i;
		}
		return fat;
	}
	
}
