package br.ucsal.cst20182.testequalidade.aula09.data;

import br.ucsal.cst20182.testequalidade.aula09.domain.User;

public class UserBuilder {
	public static final String DEFAULT_NAME = "John Smith";
	public static final String DEFAULT_ROLE = "ALUNO";
	public static final String DEFAULT_PASSWORD = "42";

	private String username;
	private String password = DEFAULT_PASSWORD;
	private String role = DEFAULT_ROLE;
	private String name = DEFAULT_NAME;

	private UserBuilder() {
	}

	public static UserBuilder aUser() {
		return new UserBuilder();
	}

	public UserBuilder comName(String name) {
		this.name = name;
		return this;
	}

	public UserBuilder comUsername(String username) {
		this.username = username;
		return this;
	}

	public UserBuilder comPassword(String password) {
		this.password = password;
		return this;
	}

	public UserBuilder semPassword() {
		this.password = null;
		return this;
	}

	public UserBuilder doTipoProfessor() {
		this.role = "PROFESSOR";
		return this;
	}

	public UserBuilder doTipoAluno() {
		this.role = "ALUNO";
		return this;
	}

	public UserBuilder doTipo(String role) {
		this.role = role;
		return this;
	}

	public UserBuilder mas() {
		return UserBuilder.aUser().doTipo(role).comName(name).comPassword(password).comUsername(username);
	}

	public User build() {
		return new User(name, username, password, role);
	}
}
