package br.ucsal.cst20182.testequalidade.aula09;

public class CalculosEspeciais {

	public CalculoUtil calculoUtil;

	public CalculosEspeciais(CalculoUtil calculoUtil) {
		this.calculoUtil = calculoUtil;
	}

	public Double calcularE(Integer n) {
		Double e = 0d;
		for (int i = 0; i <= n; i++) {
			e += 1d / calculoUtil.calcularFatorial(i);
		}
		return e;
	}

}
