package br.ucsal.cst20182.testequalidade.aula09.business;

import org.junit.Assert;
import org.junit.Test;

import br.ucsal.cst20182.testequalidade.aula09.data.TestUsers;
import br.ucsal.cst20182.testequalidade.aula09.domain.User;

public class LoginBOTest {

	@Test
	public void validarUsuarioProfessor() {
		// Dados de entrada
		// Construir uma inst�ncia da classe User

		// Alternativa 1: instanciar utilizando o construtor vazio e, utilizando
		// setter, definir os atributos. Fica muito extenso, ocupa muito lugar
		// na classe.
		// User user1 = new User();
		// user1.setName("Claudio Neiva");
		// user1.setUserName("antoniocp");
		// user1.setRole("PROFESSOR");
		// user1.setPassword("maria");

		// Alternativa 2: instanciar utilizando o construtor parametrizado.
		// Reduz
		// o tamanho do c�digo, todavia reduz tamb�m a legibilidade.
		// User user1 = new User("Claudio Neiva", "antoniocp", "maria",
		// "PROFESSOR");

		// Alternativa 3: instanciar utilizando o ObjectMother
		User user1 = TestUsers.umUsuarioProfessor();

		// Resultado esperado
		Boolean validacaoEsperada = true;

		// Executar o m�todo de teste e obter o resultado atual
		Boolean validacaoAtual = LoginBO.validarLogin(user1);

		// Comparar o resultado esperado com o resultado atual.
		Assert.assertEquals(validacaoEsperada, validacaoAtual);
	}

	@Test
	public void validar1UsuarioAlunoComObjectMother() {
		User user1 = TestUsers.umUsuarioAluno();
		Boolean validacaoEsperada = true;
		Boolean validacaoAtual = LoginBO.validarLogin(user1);
		Assert.assertEquals(validacaoEsperada, validacaoAtual);
	}

	@Test
	public void validar2UsuarioAlunoComObjectMother() {
		User user1 = TestUsers.umUsuarioAluno();
		Boolean validacaoEsperada = false;
		Boolean validacaoAtual = LoginBO.validarLogin(user1);
		Assert.assertEquals(validacaoEsperada, validacaoAtual);
	}

	@Test
	public void validar1UsuarioAlunoSemObjectMother() {
		User user1 = new User();
		user1.setName("Silvio");
		user1.setUserName("svn");
		user1.setRole("ALUNO");
		user1.setPassword("gelado");
		Boolean validacaoEsperada = true;
		Boolean validacaoAtual = LoginBO.validarLogin(user1);
		Assert.assertEquals(validacaoEsperada, validacaoAtual);
	}

	@Test
	public void validar2UsuarioAlunoSemObjectMother() {
		User user1 = new User();
		user1.setName("Silvio");
		user1.setUserName("svn");
		user1.setRole("ALUNO");
		user1.setPassword("gelado");
		Boolean validacaoEsperada = false;
		Boolean validacaoAtual = LoginBO.validarLogin(user1);
		Assert.assertEquals(validacaoEsperada, validacaoAtual);
	}

}
