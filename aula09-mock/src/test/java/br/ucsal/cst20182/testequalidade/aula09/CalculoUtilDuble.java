package br.ucsal.cst20182.testequalidade.aula09;

public class CalculoUtilDuble extends CalculoUtil {

	@Override
	public Long calcularFatorial(int n) {
		switch (n) {
		case 0:
			return 1L;
		case 1:
			return 1L;
		case 2:
			return 2L;
		case 3:
			return 6L;
		}
		return null;
	}

}
