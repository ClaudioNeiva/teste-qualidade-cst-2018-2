package br.ucsal.cst20182.testequalidade.aula09.domain;

public class User {

	private String name;
	
	private String userName;
	
	private String password;

	private String role;

	public User() {
		super();
	}

	public User(String name, String userName, String password, String role) {
		super();
		this.name = name;
		this.userName = userName;
		this.password = password;
		this.role = role;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
