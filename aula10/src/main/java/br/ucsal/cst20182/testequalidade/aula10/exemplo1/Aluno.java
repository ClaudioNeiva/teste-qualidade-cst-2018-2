package br.ucsal.cst20182.testequalidade.aula10.exemplo1;

public class Aluno {

	private Integer matricula;

	private String nome;

	private SituacaoAluno situacao;

	private Integer anoNascimento;

	public Aluno() {
		super();
	}

	public Integer getMatricula() {
		return matricula;
	}

	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public SituacaoAluno getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoAluno situacao) {
		this.situacao = situacao;
	}

	public Integer getAnoNascimento() {
		return anoNascimento;
	}

	public void setAnoNascimento(Integer anoNascimento) {
		this.anoNascimento = anoNascimento;
	}

	@Override
	public String toString() {
		return "Aluno [matricula=" + matricula + ", nome=" + nome + ", situacao=" + situacao + ", anoNascimento="
				+ anoNascimento + "]";
	}

}
