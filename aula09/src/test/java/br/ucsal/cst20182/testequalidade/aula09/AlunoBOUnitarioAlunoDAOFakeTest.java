package br.ucsal.cst20182.testequalidade.aula09;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AlunoBOUnitarioAlunoDAOFakeTest {
	
	private DateUtil dateUtil;

	private AlunoDAO alunoDAOFake;

	private AlunoBO alunoBO;

	@Before
	public void setup() {
		dateUtil = new DateUtil();
		alunoDAOFake = new AlunoDAOFake();
		alunoBO = new AlunoBO(alunoDAOFake, dateUtil);
	}
	
	@Test
	public void testarCalculoIdade() {
		
		// Definindo os dados de entrada
		Integer matricula = 5;
		Aluno alunoAtivo = AlunoBuilder.umAlunoAtivo().comMatricula(matricula)
				.nascidoEm(1998).build();
		alunoBO.atualizar(alunoAtivo);
		
		// Definindo a sa�da esperada
		Integer idadeEsperada = 20;
		
		// Executando o m�todo que est� sendo testado e obtendo o resultado atual
		Integer idadeAtual = alunoBO.calcularIdade(matricula);
		
		// Comparando o resultado esperado com o resultado atual
		Assert.assertEquals(idadeEsperada, idadeAtual);
	}

}
