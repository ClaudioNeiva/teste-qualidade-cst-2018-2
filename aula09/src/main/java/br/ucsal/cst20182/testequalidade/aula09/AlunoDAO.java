package br.ucsal.cst20182.testequalidade.aula09;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

//create table aluno
//(matricula integer not null primary key,
// nome varchar(60) not null,
// situacao varchar(20) not null,
// anoNascimento integer not null);
public class AlunoDAO extends AbstractDAO {

	public void salvar(Aluno aluno) {
		String sql = "insert into aluno (matricula, nome, situacao, anoNascimento) values (";
		sql += aluno.getMatricula() + ",";
		sql += "'" + aluno.getNome() + "',";
		sql += "'" + aluno.getSituacao() + "',";
		sql += aluno.getAnoNascimento() + ")";

		try (Statement statement = getConnection().createStatement()) {
			statement.execute(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public Aluno encontrarPorMatricula(Integer matricula) {
		String sql = "select nome, situacao, anoNascimento ";
		sql += " from aluno ";
		sql += " where matricula = " + matricula;

		try (Statement statement = getConnection().createStatement();
				ResultSet resultSet = statement.executeQuery(sql);) {
			if (resultSet.next()) {
				return resultSetToAluno(matricula, resultSet);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	private Aluno resultSetToAluno(Integer matricula, ResultSet resultSet) throws SQLException {
		String nome = resultSet.getString("nome");
		String situacao = resultSet.getString("situacao");
		Integer anoNascimento = resultSet.getInt("anoNascimento");
		Aluno aluno = new Aluno();
		aluno.setMatricula(matricula);
		aluno.setNome(nome);
		aluno.setAnoNascimento(anoNascimento);
		aluno.setSituacao(SituacaoAluno.valueOf(situacao));
		return aluno;
	}

}
